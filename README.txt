CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Unused Files menu module displays every unused files from your website, that can be filtered by file type and status.

For a full description of the project visit the project page:
http://drupal.org/project/unused_files

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/unused_files


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

 * You likely want to disable Toolbar module, since its output clashes with
 Administration menu.


CONFIGURATION
-------------

No configuration needed.


MAINTAINERS
-----------

Current maintainers:
 * Agence Opsone (agence.opsone) - http://drupal.org/user/agenceopsone
 * Noémie Kerroux (noemie.kerroux) - https://www.drupal.org/u/noemiekerroux
